# frozen_string_literal: true

require 'spec_helper'

describe Projects::Prometheus::AlertsController do
  let(:user) { create(:user) }
  let(:project) { create(:project) }
  let(:environment) { create(:environment, project: project) }
  let(:metric) { create(:prometheus_metric, project: project) }

  before do
    stub_licensed_features(prometheus_alerts: true)
    project.add_master(user)
    sign_in(user)
  end

  describe 'GET #index' do
    context 'when project has no prometheus alert' do
      it 'renders forbidden when unlicensed' do
        stub_licensed_features(prometheus_alerts: false)

        get :index, params: project_params

        expect(response).to have_gitlab_http_status(:not_found)
      end

      it 'returns an empty response' do
        get :index, params: project_params

        expect(response).to have_gitlab_http_status(200)
        expect(JSON.parse(response.body)).to be_empty
      end
    end

    context 'when project has prometheus alerts' do
      before do
        create_list(:prometheus_alert, 3, project: project, environment: environment)
      end

      it 'contains prometheus alerts' do
        get :index, params: project_params

        expect(response).to have_gitlab_http_status(200)
        expect(JSON.parse(response.body).count).to eq(3)
      end
    end
  end

  describe 'GET #show' do
    context 'when alert does not exist' do
      it 'renders 404' do
        get :show, params: project_params(id: PrometheusAlert.all.maximum(:prometheus_metric_id).to_i + 1)

        expect(response).to have_gitlab_http_status(404)
      end
    end

    context 'when alert exists' do
      let(:alert) { create(:prometheus_alert, project: project, environment: environment, prometheus_metric: metric) }

      it 'renders forbidden when unlicensed' do
        stub_licensed_features(prometheus_alerts: false)

        get :show, params: project_params(id: alert.prometheus_metric_id)

        expect(response).to have_gitlab_http_status(:not_found)
      end

      it 'renders the alert' do
        alert_params = {
          "id" => alert.id,
          "title" => alert.title,
          "query" => alert.query,
          "operator" => alert.computed_operator,
          "threshold" => alert.threshold,
          "alert_path" => Gitlab::Routing.url_helpers.project_prometheus_alert_path(project, alert.prometheus_metric_id, environment_id: alert.environment.id, format: :json)
        }

        get :show, params: project_params(id: alert.prometheus_metric_id)

        expect(response).to have_gitlab_http_status(200)
        expect(JSON.parse(response.body)).to include(alert_params)
      end
    end
  end

  describe 'POST #notify' do
    let(:notify_service) { spy }
    let(:payload) { {} }

    before do
      allow(Projects::Prometheus::Alerts::NotifyService).to receive(:new).and_return(notify_service)
    end

    it 'sends a notification for firing alerts only' do
      expect(notify_service).to receive(:execute).and_return(true)

      post :notify, params: project_params(payload), session: { as: :json }

      expect(response).to have_gitlab_http_status(200)
    end

    it 'renders unprocessable entity if notification fails' do
      expect(notify_service).to receive(:execute).and_return(false)

      post :notify, params: project_params, session: { as: :json }

      expect(response).to have_gitlab_http_status(422)
    end
  end

  describe 'POST #create' do
    it 'renders forbidden when unlicensed' do
      stub_licensed_features(prometheus_alerts: false)

      post :create, params: project_params(
        operator: ">",
        threshold: "1",
        environment_id: environment.id,
        prometheus_metric_id: metric.id
      )

      expect(response).to have_gitlab_http_status(:not_found)
    end

    it 'creates a new prometheus alert' do
      schedule_update_service = spy
      alert_params = {
        "title" => metric.title,
        "query" => metric.query,
        "operator" => ">",
        "threshold" => 1.0
      }

      allow(::Clusters::Applications::ScheduleUpdateService).to receive(:new).and_return(schedule_update_service)

      post :create, params: project_params(
        operator: ">",
        threshold: "1",
        environment_id: environment.id,
        prometheus_metric_id: metric.id
      )

      expect(schedule_update_service).to have_received(:execute)
      expect(response).to have_gitlab_http_status(200)
      expect(JSON.parse(response.body)).to include(alert_params)
    end

    context 'with a project non-specific environment' do
      let(:environment) { create(:environment) }

      it 'returns 204 status' do
        post :create, params: project_params(
          operator: ">",
          threshold: "1",
          environment_id: environment.id,
          prometheus_metric_id: metric.id
        )

        expect(response).to have_gitlab_http_status(:no_content)
      end
    end
  end

  describe 'POST #update' do
    let(:schedule_update_service) { spy }
    let(:alert) { create(:prometheus_alert, project: project, environment: environment, prometheus_metric: metric) }

    before do
      allow(::Clusters::Applications::ScheduleUpdateService).to receive(:new).and_return(schedule_update_service)
    end

    it 'renders forbidden when unlicensed' do
      stub_licensed_features(prometheus_alerts: false)

      put :update, params: project_params(id: alert.prometheus_metric_id, operator: "<")

      expect(response).to have_gitlab_http_status(:not_found)
    end

    it 'updates an already existing prometheus alert' do
      alert_params = {
        "id" => alert.id,
        "title" => alert.title,
        "query" => alert.query,
        "operator" => "<",
        "threshold" => alert.threshold,
        "alert_path" => Gitlab::Routing.url_helpers.project_prometheus_alert_path(project, alert.prometheus_metric_id, environment_id: alert.environment.id, format: :json)
      }

      expect do
        put :update, params: project_params(id: alert.prometheus_metric_id, operator: "<")
      end.to change { alert.reload.operator }.to("lt")

      expect(schedule_update_service).to have_received(:execute)
      expect(response).to have_gitlab_http_status(200)
      expect(JSON.parse(response.body)).to include(alert_params)
    end
  end

  describe 'DELETE #destroy' do
    let(:schedule_update_service) { spy }
    let!(:alert) { create(:prometheus_alert, project: project, prometheus_metric: metric) }

    before do
      allow(::Clusters::Applications::ScheduleUpdateService).to receive(:new).and_return(schedule_update_service)
    end

    it 'renders forbidden when unlicensed' do
      stub_licensed_features(prometheus_alerts: false)

      delete :destroy, params: project_params(id: alert.prometheus_metric_id)

      expect(response).to have_gitlab_http_status(:not_found)
    end

    it 'destroys the specified prometheus alert' do
      expect do
        delete :destroy, params: project_params(id: alert.prometheus_metric_id)
      end.to change { PrometheusAlert.count }.from(1).to(0)

      expect(schedule_update_service).to have_received(:execute)
    end
  end

  def project_params(opts = {})
    opts.reverse_merge(namespace_id: project.namespace, project_id: project)
  end
end
